using System.Collections;

namespace CorpBlog.Models;

public interface IPostService {
  Task<IEnumerable> Published();
  CorpBlogDbContext Db { get; }
}