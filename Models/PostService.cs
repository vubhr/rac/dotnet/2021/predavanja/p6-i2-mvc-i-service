using System.Collections;
using Microsoft.EntityFrameworkCore;

namespace CorpBlog.Models;

public class PostService : IPostService {
  public CorpBlogDbContext Db { get; }

  public PostService(CorpBlogDbContext context) {
    Db = context;
  }

  public async Task<IEnumerable> Published() {
    return await Db.Posts.Where(post => post.Published == true).ToListAsync();
  }

}